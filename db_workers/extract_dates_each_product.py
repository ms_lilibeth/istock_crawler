import pymysql
from pymysql import MySQLError
import getpass
import sys
import pandas as pd

print('Enter the password for mysql db (user: ms_lilibeth)...')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    sys.exit()

authors_photo = []
authors_video = []
authors_illustration = []

dates_photo = []
dates_video = []
dates_illustration = []

authors = []
dates = []
try:
    with connection.cursor() as cursor:
        sql = "SELECT `authorID`, `registration_date` from `p_stat` ORDER BY `registration_date`"
        cursor.execute(sql)
        for row in cursor:
            authors.append(row['authorID'])
            dates.append(row['registration_date'])

        sql = "SELECT `authorID`, `registration_date` from `p_stat` WHERE EXISTS" \
              " (SELECT 1 FROM `photos` WHERE photos.authorID=p_stat.authorID)" \
              " ORDER BY `registration_date`"
        cursor.execute(sql)
        for row in cursor:
            authors_photo.append(row['authorID'])
            dates_photo.append(row['registration_date'])

        sql = "SELECT `authorID`, `registration_date` from `p_stat` WHERE EXISTS" \
              " (SELECT 1 FROM `illustrations` WHERE illustrations.authorID=p_stat.authorID)" \
              " ORDER BY `registration_date`"
        cursor.execute(sql)
        for row in cursor:
            authors_illustration.append(row['authorID'])
            dates_illustration.append(row['registration_date'])

        sql = "SELECT `authorID`, `registration_date` from `p_stat` WHERE EXISTS" \
              " (SELECT 1 FROM `video` WHERE video.authorID=p_stat.authorID)" \
              " ORDER BY `registration_date`"
        cursor.execute(sql)
        for row in cursor:
            authors_video.append(row['authorID'])
            dates_video.append(row['registration_date'])

except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)

pd.DataFrame({'ID': authors_photo, 'registration_date': dates_photo}).to_csv('registrations_photo.csv', index=False)
pd.DataFrame({'ID': authors_video, 'registration_date': dates_video}).to_csv('registrations_video.csv', index=False)
pd.DataFrame({'ID': authors_illustration, 'registration_date': dates_illustration}).to_csv('registrations_illustration.csv', index=False)
pd.DataFrame({'ID': authors, 'registration_date': dates}).to_csv('registrations_all.csv', index=False)
