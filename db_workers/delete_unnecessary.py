import pymysql
from pymysql import MySQLError
import getpass
import sys

output_file = 'delete_unnecessary.log'

print('Enter the password for mysql db (user: ms_lilibeth)...')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    with open(output_file, 'a') as f:
        f.write(err_msg)
    sys.exit()

to_delete = [6676708, 12749056, 11541548, 2184277, 10296483, 10669655, 460341, 9633007, 12448976, 6547387, 13871039,
14557163, 14703319, 11601756]

try:
    with connection.cursor() as cursor:
        for id_ in to_delete:
            sql = "DELETE FROM `p_stat` WHERE `authorID`=%s"
            cursor.execute(sql, (id_))
        connection.commit()
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    with open(output_file, 'a') as f:
        f.write(err_msg)
