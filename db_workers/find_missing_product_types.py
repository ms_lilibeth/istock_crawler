from pandas import read_csv
import pymysql
from pymysql import MySQLError
import getpass
import sys

output_file = 'product_types_MISSING'

print('Enter the password for mysql db (user: ms_lilibeth)...')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    with open(output_file, 'a') as f:
        f.write(err_msg)
    sys.exit()

query_result = set()
try:
    with connection.cursor() as cursor:
        sql = "SELECT `authorID` from `photos`"
        cursor.execute(sql)
        for row in cursor:
            query_result.add(row['authorID'])

        sql = "SELECT `authorID` from `illustrations`"
        cursor.execute(sql)
        for row in cursor:
            query_result.add(row['authorID'])

        sql = "SELECT `authorID` from `video`"
        cursor.execute(sql)
        for row in cursor:
            query_result.add(row['authorID'])

except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    with open(output_file, 'a') as f:
        f.write(err_msg)

result = []
PORTFOLIO_URLS = read_csv("istock.csv", sep=';')
for index, row in PORTFOLIO_URLS.iterrows():
    if row['ID'] not in query_result:
        result.append(row['ID'])

with open(output_file, 'a') as f:
    for item in result:
        f.write(str(item)+'\n')