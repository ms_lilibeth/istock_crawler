# fail: распознает юзернейм 8e88graphic как double и обрезает его
import pymysql
from pymysql import MySQLError
import getpass
import sys
import pandas as pd

csv_info = pd.read_csv('usernames.csv')

print('Enter the password for mysql db (user: ms_lilibeth)...')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    sys.exit()

for index, row in csv_info.iterrows():
    try:
        with connection.cursor() as cursor:
            sql = "UPDATE `p_stat` SET `username`='%s' WHERE `authorID`=%s"
            cursor.execute(sql, (row['ID'], row['username']))

    except MySQLError as e:
        err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
        print(err_msg)

