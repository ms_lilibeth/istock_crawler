import pymysql
from pymysql import MySQLError
import getpass
import sys
import pandas as pd

print('Enter the password for mysql db (user: ms_lilibeth)...')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    sys.exit()

all_info = {} # key - authorID, value = {'files': ..., 'photo': ..., 'video': ..., 'illustration': ...}

try:
    with connection.cursor() as cursor:
        sql = "SELECT `authorID`, `worksCount` from `p_stat` ORDER BY `worksCount` DESC"
        cursor.execute(sql)
        for row in cursor:
            all_info[row['authorID']] = {'files': row['worksCount']}

        sql = "select photos.authorID, photos.qty from photos inner join p_stat on photos.authorID=p_stat.authorID"
        cursor.execute(sql)
        for row in cursor:
            all_info[row['authorID']]['photo'] = row['qty']

        sql = "select video.authorID, video.qty from video inner join p_stat on video.authorID=video.authorID"
        cursor.execute(sql)
        for row in cursor:
            all_info[row['authorID']]['video'] = row['qty']

        sql = "select illustrations.authorID, illustrations.qty from illustrations " \
              "inner join p_stat on p_stat.authorID=illustrations.authorID"
        cursor.execute(sql)
        for row in cursor:
            all_info[row['authorID']]['illustration'] = row['qty']

except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)

authors = []
files = []
photo = []
video = []
illustration = []

for a in all_info:
    authors.append(a)
    files.append(all_info[a]['files'])
    if 'photo' in all_info[a]:
        photo.append(all_info[a]['photo'])
    else:
        photo.append(0)
    if 'video' in all_info[a]:
        video.append(all_info[a]['video'])
    else:
        video.append(0)
    if 'illustration' in all_info[a]:
        illustration.append(all_info[a]['illustration'])
    else:
        illustration.append(0)


def convert(x):
    try:
        return x.astype(int)
    except:
        return x

df = pd.DataFrame({"ID": authors, "files": files, "photo": photo,
                   "video": video, "illustration": illustration})
df.apply(convert)
df.to_csv('product_types_data.csv', index=False,
          columns=['ID', 'files', 'photo', 'video', 'illustration'])
