import pymysql
from pymysql import MySQLError
import getpass
import sys
import pandas as pd

print('Enter the password for mysql db (user: ms_lilibeth)...')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    sys.exit()

aliases_csv = pd.read_csv('achievements_aliases', sep=';')
aliases = {}
for index, row in aliases_csv.iterrows():
    aliases[row['a_text']] = row['alias']

ach_data = {}
try:
    with connection.cursor() as cursor:
        sql = "SELECT `authorID`, `a_text` from `achievements`"
        cursor.execute(sql)
        for row in cursor:
            a = row['authorID']
            if a in ach_data:
                ach_data[a].append(aliases[row['a_text']])
            else:
                ach_data[a] = [aliases[row['a_text']]]

        for a in ach_data:
            s = ','.join(ach_data[a])
            sql = "INSERT INTO `achievements_set`(`authorID`,`ach`) VALUES (%s, %s)"
            cursor.execute(sql, (a, s))
    connection.commit()

except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)

