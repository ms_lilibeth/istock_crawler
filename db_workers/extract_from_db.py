import pymysql
from pymysql import MySQLError
import getpass
import sys
import pandas as pd
import numpy as np

print('Enter the password for mysql db (user: ms_lilibeth)...')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    sys.exit()

ids_p_stat = []
ids_photo = []
ids_video = []
ids_illstr = []
ids_achievements = []

usernames = []
worksCount = []
downloads = []
dates_oldest = []
dates_newest = []
reg_dates = []

dates_oldest_photo = []
dates_newest_photo = []
dates_oldest_video = []
dates_newest_video = []
dates_oldest_illstr = []
dates_newest_illstr = []

qty_photo = []
qty_video = []
qty_illstr = []

achievements = []

# main info
usernames_csv = pd.read_csv("usernames.csv")
try:
    with connection.cursor() as cursor:
        sql = "SELECT * from `p_stat` ORDER BY `downloads` DESC"
        cursor.execute(sql)
        for row in cursor:
            ids_p_stat.append(row['authorID'])
            worksCount.append(row['worksCount'])
            if row['downloads'] is not None:
                downloads.append(int(row['downloads']))
            else:
                downloads.append(0)
            dates_oldest.append(row['date_oldest'])
            dates_newest.append(row['date_newest'])
            reg_dates.append(row['registration_date'])
            csv_filter_result = usernames_csv.loc[usernames_csv['ID'] == row['authorID']]
            usernames.append(csv_filter_result.iloc[0]['username'])

except MySQLError as e:
    err_msg = 'p_stat: Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)

#photo
try:
    with connection.cursor() as cursor:
        sql = "SELECT * from `photos`"
        cursor.execute(sql)
        for row in cursor:
            ids_photo.append(row['authorID'])
            if row['qty'] is not None:
                qty_photo.append(int(row['qty']))
            else:
                qty_photo.append(0)
            dates_oldest_photo.append(row['date_oldest'])
            dates_newest_photo.append(row['date_newest'])

except MySQLError as e:
    err_msg = 'photo: Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)

#video
try:
    with connection.cursor() as cursor:
        sql = "SELECT * from `video`"
        cursor.execute(sql)
        for row in cursor:
            ids_video.append(row['authorID'])
            if row['qty'] is not None:
                qty_video.append(int(row['qty']))
            else:
                qty_video.append(0)
            dates_oldest_video.append(row['date_oldest'])
            dates_newest_video.append(row['date_newest'])

except MySQLError as e:
    err_msg = 'video: Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)

# illustration
try:
    with connection.cursor() as cursor:
        sql = "SELECT * from `illustrations`"
        cursor.execute(sql)
        for row in cursor:
            ids_illstr.append(row['authorID'])
            if row['qty'] is not None:
                qty_illstr.append(int(row['qty']))
            else:
                qty_illstr.append(0)
            dates_oldest_illstr.append(row['date_oldest'])
            dates_newest_illstr.append(row['date_newest'])

except MySQLError as e:
    err_msg = 'illustration: Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)

# achievements
try:
    with connection.cursor() as cursor:
        sql = "SELECT * from `achievements_set`"
        cursor.execute(sql)
        for row in cursor:
            ids_achievements.append(row['authorID'])
            achievements.append(row['ach'])

except MySQLError as e:
    err_msg = 'achievements: Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)

def convert(x):
    try:
        return x.astype(int)
    except:
        return x
ids_photo = np.array(ids_photo, dtype='int')
ids_video = np.array(ids_video, dtype='int')
ids_p_stat = np.array(ids_p_stat, dtype='int')
ids_illstr = np.array(ids_illstr, dtype='int')

qty_photo = np.array(qty_photo, dtype='int')
qty_video = np.array(qty_video, dtype='int')
qty_illstr = np.array(qty_illstr, dtype='int')
worksCount = np.array(worksCount, dtype='int')
downloads = np.array(downloads, dtype='int')
df = pd.DataFrame({'ID': ids_p_stat, 'username': usernames, 'files': worksCount, 'downloads': downloads, 'date_oldest': dates_oldest,
                   'date_newest': dates_newest, 'reg_date': reg_dates})
df_photo = pd.DataFrame({'ID': ids_photo, 'photo': qty_photo, 'date_oldest_photo': dates_oldest_photo,
                         'date_newest_photo': dates_newest_photo})
df_video = pd.DataFrame({'ID': ids_video, 'video': qty_video, 'date_oldest_video': dates_oldest_video,
                         'date_newest_video': dates_newest_video})
df_illstr = pd.DataFrame({'ID': ids_illstr, 'illustration': qty_illstr, 'date_oldest_illustration': dates_oldest_illstr,
                          'date_newest_illustration': dates_newest_illstr})
df_achievements = pd.DataFrame({'ID': ids_achievements, 'achievements': achievements})

df = df.merge(df_photo, how='outer', on='ID')
df = df.merge(df_video, how='outer', on='ID')
df = df.merge(df_illstr, how='outer', on='ID')
df = df.merge(df_achievements, how='outer', on='ID')

# df[['photo', 'video', 'illustration', 'files', 'downloads', 'ID']].fillna(0, inplace=True)
df['photo'].fillna(0, inplace=True)
df['video'].fillna(0, inplace=True)
df['illustration'].fillna(0, inplace=True)
df['files'].fillna(0, inplace=True)
df['downloads'].fillna(0, inplace=True)
df['ID'].fillna(0, inplace=True)

df['photo'] = df['photo'].astype(int)
df['video'] = df['video'].astype(int)
df['illustration'] = df['illustration'].astype(int)
df['files'] = df['files'].astype(int)
df['downloads'] = df['downloads'].astype(int)
df['ID'] = df['ID'].astype(int)
df.apply(convert)
df.to_csv('summary_table_fullest.csv', sep=';', index=False,
                                              columns=['username', 'ID', 'reg_date', 'downloads', 'files', 'photo', 'video',
                                                       'illustration', 'date_oldest', 'date_newest', 'date_oldest_photo',
                                                       'date_newest_photo', 'date_oldest_video', 'date_newest_video',
                                                       'date_oldest_illustration', 'date_newest_illustration', 'achievements'])