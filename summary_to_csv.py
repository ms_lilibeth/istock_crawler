# -*- coding: utf-8 -*-
HOMEDIR = '~/GIT/istock_crawler/data/'
from datetime import datetime
import pandas as pd
import math

photo_threshhold = 219
video_threshhold = 8
illstr_threshhold = 15

df = pd.read_csv(HOMEDIR+'summary_table_fullest.csv', sep=';')


def convert(x):
    try:
        return x.astype(int)
    except:
        return x

date_last = datetime.today()

ages = [str((date_last - datetime.strptime(d, '%Y-%m-%d')).days//7) if type(d) is str else "" for d in df['reg_date']]
is_photographer = [1 if p >= photo_threshhold else 0 for p in df['photo']]
is_videographer = [1 if p >= video_threshhold else 0 for p in df['video']]
is_illustrator = [1 if p >= illstr_threshhold else 0 for p in df['illustration']]

df['age_in_weeks'] = pd.Series(ages, index=df.index)
df['is_photographer'] = pd.Series(is_photographer, index=df.index)
df['is_videographer'] = pd.Series(is_videographer, index=df.index)
df['is_illustrator'] = pd.Series(is_illustrator, index=df.index)

df = df.rename(index=str, columns={"username": "name", "ID": "user_id"})
# df['age_in_weeks'] = df['age_in_weeks'].astype(int)
df.to_csv("summary_specializations.csv", index=False, columns=['name', 'user_id', 'files', 'downloads', 'reg_date',
                                                    'age_in_weeks', 'is_photographer', 'is_videographer',
                                                    'is_illustrator'])