from os import listdir
from os.path import isfile, join
import pandas as pd

path_img = '/home/ms_lilibeth/download_photos/1010902'
path_csv = '/home/ms_lilibeth/download_photos/1010902.csv'
imgs = [f[:-4] for f in listdir(path_img) if isfile(join(path_img, f))]
imgs = list(map(int, imgs))

df = pd.read_csv(path_csv)

df = df[~df['gm_id'].isin(imgs)]
df.to_csv('/home/ms_lilibeth/download_photos/1010902_new.csv', index=False)
