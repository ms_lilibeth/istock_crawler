import pandas as pd

ids = []
usernames = []

rows = pd.read_csv("istock.csv", sep=';')
for index, row in rows.iterrows():
    usernames.append(row['URL'].rpartition('/')[-1])
    ids.append(row['ID'])

pd.DataFrame({'ID': ids, 'username': usernames}).to_csv("usernames.csv", index=False)