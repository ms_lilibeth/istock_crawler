# -*- coding: utf-8 -*-
HOMEDIR = '~/GIT/istock_crawler/data/'
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from dateutil.relativedelta import relativedelta

data = pd.read_csv(HOMEDIR+'product_types_data.csv')


def convert(x):
    try:
        return x.astype(int)
    except:
        return x

data.apply(convert)
typeofdata = 'illustration'

data = data[typeofdata].tolist()
data = [d for d in data if d != 0]
data.sort()
index = int(len(data)//5)

print('Index: ', index)
print('Num of works: ', data[index])
print('Num of authors: ', len(data))


