# HOMEDIR = '~/GIT/istock_crawler/'

import pandas as pd
from numpy import random, mean, var, std
reg_csv = pd.read_csv('regs-by-month-all.csv', sep=';')
january_reg = 0
regs = []
for index, row in reg_csv.iterrows():
    if row['month'] == 'January 2016':
        january_reg = row['regs_num']
    else:
        regs.append(row['regs_num'])

_mean = mean(regs)
_std = std(regs)
print("Mean: %s, regs num in January: %s" % (_mean, january_reg))
print("January, deviation: ", january_reg - _mean)
print("std: %d, 3*std: %d" % (_std, 3*_std))
if january_reg - _mean > 3*_std:
    print("Something interesting happens")
else:
    print("Nothing interesting")
