# -*- coding: utf-8 -*-
HOMEDIR = '~/GIT/istock_crawler/data/'
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from dateutil.relativedelta import relativedelta
import matplotlib


def autolabel(rects,ax):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.01*height,
                '%d%%' % int(height*100),
                ha='center', va='bottom', fontsize=12)

data = pd.read_csv(HOMEDIR+'product_types_data.csv')


def convert(x):
    try:
        return x.astype(int)
    except:
        return x

data.apply(convert)
min_size = 0
max_size = max(data[['files', 'photo', 'video', 'illustration']].max(axis=1))
step = 1
periods = list(range(min_size, max_size+step, step))
files = np.zeros(len(periods), dtype='int32') # кол-во людей, имеющих размер портфолио от N до N+1 тысячи
photo = np.zeros(len(periods), dtype='int32')
video = np.zeros(len(periods), dtype='int32')
illustration = np.zeros(len(periods))
print(data['illustration'].tolist())
for index, row in data.iterrows():
    if row['files'] != 0 and row['files'] // step < len(files)-1:
        files[row['files'] // step] += 1
    else:
        files[-1] += 1

    if row['photo'] != 0 and row['photo'] // step < len(photo)-1:
        photo[row['photo'] // step] += 1
    else:
        photo[-1] += 1

    if row['video'] != 0 and row['video'] // step < len(video)-1:
        video[row['video'] // step] += 1
    else:
        video[-1] += 1

    if row['illustration'] != 0 and row['illustration'] // step < len(illustration)-1:
        illustration[row['illustration'] // step] += 1
    else:
        illustration[-1] += 1

cut_to = int(500/step + 1)
periods = periods[1:cut_to]
files = files[1:cut_to]
photo = photo[1:cut_to]
video = video[1:cut_to]
illustration = illustration[1:cut_to]

matplotlib.rc('font', family='Arial')
f, (ax1, ax2, ax3, ax4) = plt.subplots(4, sharex=True, sharey=True)
f.subplots_adjust(hspace=0)
files = files.tolist()
photo = photo.tolist()
video = video.tolist()
illustration = illustration.tolist()

rects1 = ax1.bar(periods, files, width=step)
rects2 = ax2.bar(periods, photo, width=step)
rects3 = ax3.bar(periods, video, width=step)
rects4 = ax4.bar(periods, illustration, width=step)

axes = plt.gca()
axes.set_ylim([0, 80])

ax1.set_title("All files", y=0.5)
ax2.set_title("Photos", y=0.5)
ax3.set_title("Video", y=0.5)
ax4.set_title("Illustration", y=0.5)

plt.show()