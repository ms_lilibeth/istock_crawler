# -*- coding: utf-8 -*-
HOMEDIR = '~/GIT/istock_crawler/data/'
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from dateutil.relativedelta import relativedelta
import matplotlib
def autolabel(rects, ax):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.01*height,
                '%d%%' % int(height*100),
                ha='center', va='bottom', fontsize=12)

dates = pd.read_csv(HOMEDIR+'registrations_all.csv')
date_first_str = dates['registration_date'].iloc[0]
date_last_str = dates['registration_date'].iloc[-1]


date_first = datetime.strptime(date_first_str, "%Y-%m-%d")
date_last = datetime.strptime(date_last_str, "%Y-%m-%d")
periods = []

period_next = date_first
periods.append(period_next)
delta = relativedelta(months=1)
while period_next < date_last:
    period_next = period_next + delta
    periods.append(period_next)

period_index = 0
r_num = np.zeros(len(periods)) # number of registered people
pr_d = date_last
for index, row in dates.iterrows():
    d = row['registration_date']
    d = datetime.strptime(d, "%Y-%m-%d")
    if period_index == len(periods)-1:  # на последнем элементе
        r_num[period_index] += 1
        continue
    if periods[period_index] <= d < periods[period_index+1]:
        r_num[period_index] += 1
    else:
        period_index += 1
    if d < pr_d:
        print('Halt! Previous date: %s, Current date: %s, Index: %s' % (pr_d.date(), d.date(), period_index))
    pr_d = d

# plt.xticks(np.arange(0, len(periods)), [str(periods[i].date()) for i in range(len(periods))],
#            rotation='vertical', fontsize=3)
# plt.bar(np.arange(0, len(periods)), r_num, width=1)
# plt.savefig('register-by-month.png', dpi=400, figsize=(6, 18), bbox_inches='tight', orientation='landscape')
# plt.show()

# ------------- regs by month to csv
# pr = []
# for p in periods:
#     pr.append(p.strftime("%B %Y"))
# df = pd.DataFrame({'month': pr, 'regs_num': r_num})
#
# def convert(x):
#     try:
#         return x.astype(int)
#     except:
#         return x
#
# df.apply(convert).to_csv(HOMEDIR+"regs-by-month-all.csv", sep=';', index=False)
# exit()
# ----------------------------------------

matplotlib.rc('font', family='Arial')
f, ax = plt.subplots(1, sharex=True, sharey=True)
f.subplots_adjust(hspace=0)
rects = ax.bar(np.arange(0, 2*len(periods),step=2), r_num, width=2, linewidth=0, color='r')
plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
def autolabel(rects,ax):
    # attach some text labels
    i=0
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.01*height,
                periods[i].strftime("%B %Y"),
                ha='center', va='bottom', rotation='vertical', fontsize=3)
        i += 1

autolabel(rects, ax)
plt.xticks(np.arange(0, 2*len(periods), step=2), [str(periods[i].date()) for i in range(len(periods))],
           rotation='vertical', fontsize=3)
# plt.savefig('register-by-month-illustration.png', dpi=400, figsize=(6, 18), bbox_inches='tight', orientation='landscape')
plt.show()