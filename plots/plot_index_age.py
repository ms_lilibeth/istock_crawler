# -*- coding: utf-8 -*-
HOMEDIR = '~/GIT/istock_crawler/data/'
from datetime import datetime
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import math
import json

df = pd.read_csv(HOMEDIR+'summary_fullest.csv', sep=';')
# df = df.loc[df['is_photographer'] == 1]

dates = df['reg_date'].tolist()
date_last = datetime.strptime('2016-06-02', '%Y-%m-%d')
dates = [datetime.strptime(d, '%Y-%m-%d') if type(d) is str else None for d in dates]
df['reg_date'] = pd.Series(dates, index=df.index)
df = df.loc[df['reg_date'] >= datetime.strptime("2012-01-01", '%Y-%m-%d')]
# df = df.loc[datetime.strptime("2008-01-01", '%Y-%m-%d') <= df['reg_date']]

ages = df['age_in_weeks'].tolist()
downloads = df['downloads'].tolist()
files = df['files'].tolist()
authors = df['username'].tolist()
# photos = df['photo'].tolist()

downloads_sum = df['downloads'].sum()
files_sum = df['files'].sum()

data = [files[i] / ages[i] for i in range(len(ages))]

print(downloads_sum)
print(files_sum)

# print('shironosov: ', data[authors.index('shironosov')])
# print('mediaphotos: ', data[authors.index('mediaphotos')])
authors = np.array(authors)
data = np.array(data)
ages = np.array(ages)

args = np.argsort(data)

authors_cut = [authors[a] for a in args if not math.isnan(data[a])][-50:][::-1]
data_cut = [data[a] for a in args if not math.isnan(data[a])][-50:][::-1]
ages_cut = [ages[a] for a in args if not math.isnan(data[a])][-50:][::-1]

plot = True
if plot:
    plt.plot(ages, data, 'ro')
    plt.title("Files per week", y=0.8, x=0.3)
    axes = plt.gca()
    axes.set_xlim([0, 700])
    # axes.set_ylim([0, 700])
    plt.tight_layout()

    for (i, element) in enumerate(data_cut):
        x_coord = ages_cut[i]
        y_coord = element + 3
        # plt.scatter(x_coord, y_coord)
        plt.text(x_coord, y_coord, authors_cut[i], fontsize=10, rotation=50)
    plt.show()

authors = authors_cut
data = data_cut
ages = ages_cut

write = False
if write:
    s = json.dumps(authors)
    filename = "ill-week-top-50-since-2012"
    with open(filename+".json", 'w') as f:
        f.write(s)
    with open("LIST_" + filename + ".txt", 'w') as f:
        for a in authors:
            f.write(a)
            f.write("\n")

    i=0
    with open(filename + ".txt", 'w') as f:
        for a in authors:
            f.write(a), f.write("\n")
            tmp = df.loc[df['username'] == a].iloc[0]
            f.write("%s      %s     %s     %s %s %s\n "
                    % ('ID', 'illustration_num', 'reg_date', 'is_photographer', 'is_videographer',
                       'is_illustrator'))
            f.write("%d   %d    %s      %d       %d      %d" % (tmp['ID'], tmp['illustration'],
                                              tmp['reg_date'].date(), tmp['is_photographer'], tmp['is_videographer'],
                                              tmp['is_illustrator']))
            f.write("\n")
            f.write('Age in weeks: %d \n' % tmp['age_in_weeks'])
            f.write('Illustration per week: %.5f' % (tmp['illustration']/tmp['age_in_weeks']))
            f.write('\n ---------------------------------------------------------------------\n\n')
            i += 1