# -*- coding: utf-8 -*-
HOMEDIR = '~/GIT/istock_crawler/data/'
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from dateutil.relativedelta import relativedelta
import matplotlib
import math

def autolabel(rects,ax):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.01*height,
                '%d%%' % int(height*100),
                ha='center', va='bottom', fontsize=12)

data = pd.read_csv(HOMEDIR+'product_types_data.csv')


def convert(x):
    try:
        return x.astype(int)
    except:
        return x

data.apply(convert)
min_value = 0
# max_value = max(data[['files', 'photo', 'video', 'illustration']].max(axis=1))
max_value = 100
step = 1
periods = list(range(min_value, max_value + step, step))
# files = np.zeros(len(periods), dtype='int32')
photo = np.zeros(len(periods), dtype='int32')
video = np.zeros(len(periods), dtype='int32')
illustration = np.zeros(len(periods), dtype='int32')
for index, row in data.iterrows():
    # if row['files'] // step < len(files)-1:
    #     files[row['files'] // step] += 1
    # else:
    #     files[-1] += 1
    tmp = int(100*row['photo'] // row['files'])
    if tmp <= 100:
        photo[tmp // step] += 1
    else:
        photo[-1] += 1
    # if tmp // step < len(photo)-1:
    #     photo[tmp // step] += 1
    # else:
    #     photo[-1] += 1

    tmp = int(100*row['video'] // row['files'])
    if tmp <= 100:
        video[tmp // step] += 1
    else:
        video[-1] += 1
    # if row['video'] // step < len(video)-1:
    #     video[row['video'] // step] += 1
    # else:
    #     video[-1] += 1
    tmp = int(100*row['illustration'] // row['files'])
    if tmp <= 100:
        illustration[tmp // step] += 1
    else:
        illustration[-1] += 1
    # if row['illustration'] // step < len(illustration)-1:
    #     illustration[row['illustration'] // step] += 1
    # else:
    #     illustration[-1] += 1

matplotlib.rc('font', family='Arial')
f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=True)
f.subplots_adjust(hspace=0)
# files = files.tolist()
# photo = photo.tolist()
# video = video.tolist()
# illustration = illustration.tolist()
# cut_to = int(500/step + 1)
# periods = periods[:cut_to]
# files = files[:cut_to]
# photo = photo[:cut_to]
# video = video[:cut_to]
# illustration = illustration[:cut_to]
# rects1 = ax1.bar(periods, files, width=1000)
rects1 = ax1.bar(periods, photo)
rects2 = ax2.bar(periods, video)
rects3 = ax3.bar(periods, illustration)

axes = plt.gca()
axes.set_ylim([0, 300])
axes.set_xlim([0, 100])

# ax1.set_title("All files")
ax1.set_title("Photos", y=0.5)
ax2.set_title("Video", y=0.5)
ax3.set_title("Illustration", y=0.5)
# ax1.plot(periods, files)
# ax1.plot(periods, photo)
# ax2.plot(periods, video)
# ax3.plot(periods, illustration)
plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
def autolabel(rects,ax):
    # attach some text labels
    i=0
    for rect in rects:
        height = rect.get_height()
        if i <len(periods)-1:
            ax.text(rect.get_x() + rect.get_width()/2., 1.1*height + 3,
                    ("%d") % (periods[i]),
                    ha='center', va='bottom', rotation='vertical', fontsize=8)
        else:
            ax.text(rect.get_x() + rect.get_width() / 2., 1.1*height + 3,
                    (">%d") % (periods[-1]),
                    ha='center', va='bottom', rotation='vertical', fontsize=8)
        i += 1

autolabel(rects1, ax1)
autolabel(rects2, ax2)
autolabel(rects3, ax3)
# autolabel(rects4, ax4)
# plt.xticks(np.arange(0, 2*len(periods), step=2), [str(periods[i].date()) for i in range(len(periods))],
#            rotation='vertical', fontsize=3)
# plt.savefig('product-type-percent.png', dpi=400, bbox_inches='tight', orientation='landscape')
plt.show()