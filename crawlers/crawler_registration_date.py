import requests
import pandas as pd
import json
import pymysql
from pymysql import MySQLError
import getpass
import time
from datetime import datetime
import sys
from bs4 import BeautifulSoup
import re


class ParseException(Exception):
    def __init__(self, message):
        self.code = 1244
        self.message = message

URL = 'https://www.istockphoto.com/profile/'

output_file = 'crawler_reg_date.log'
with open(output_file, 'a') as f:
    f.write(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'))
    f.write('\n')

print('Enter the password for mysql db (user: ms_lilibeth) \n')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    with open(output_file, 'a') as f:
        f.write(err_msg)
    sys.exit()


def get_date(username):
    result = {}
    url = URL + username
    r = requests.get(url)
    parsed_html = BeautifulSoup(r.text, 'html.parser')
    r.raise_for_status()  # raises requests.exceptions.HTTPError

    p = parsed_html.find_all('p', {'class': 'nm'})
    exp = re.compile("Member since: .*")
    p_suitable = []
    for _p in p:
        if exp.match(_p.getText()):
            p_suitable.append(_p.getText())
    if len(p_suitable) > 1:
        raise ParseException("Something strange was found: len(p_suitable) > 1")
    if len(p_suitable) == 0:
        raise ParseException("Downloads was not found")

    return p_suitable[0][14:]

def convert_date(date_str):
    date = datetime.strptime(date_str, "%B %Y")
    return date.date()

def write_to_database(info):
    # Connect to the database
    with connection.cursor() as cursor:
        sql = "UPDATE `p_stat` SET `registration_date`=%s WHERE `authorID`=%s"
        cursor.execute(sql, (info['date'], info['ID']))
    connection.commit()

csv_info = pd.read_csv("reg_dates_missing.csv")
result = []
for index, row in csv_info.iterrows():
    try:
        date = get_date(row['username'])
    except requests.exceptions.HTTPError:
        err_msg = 'Author %s: GET request failed' % row['username']
        print(err_msg)
        with open(output_file, 'a') as f:
            f.write(err_msg)
        continue
    except ParseException:
        err_msg = 'Author %s: Parse exception' % row['username']
        with open(output_file, 'a') as f:
            f.write(err_msg)
        continue
    date = convert_date(date)
    info = {'ID': row['ID'], 'date': date, 'name': row['username']}
    try:
        result.append(info)
        write_to_database(info)
    except MySQLError as e:
        err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
        print(err_msg)
        with open(output_file, 'a') as f:
            f.write(err_msg)
        continue
    print('\r' + str(index), end='')
ids = []
names = []
dates = []
for i in result:
    ids.append(i['ID'])
    names.append(i['name'])
    dates.append(i['date'])

pd.DataFrame({'user_id': ids, 'username': names, 'reg_date': dates}).to_csv("reg_dates_missing_found.csv", index=False)

