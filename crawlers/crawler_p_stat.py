# nice service: https://rest.istockphoto.com/api/v1/assets/<gm_id>
# доработать, добавить кастомный exception, как в других скриптах
import requests
from pandas import read_csv
import json
import pymysql
from pymysql import MySQLError
import getpass
import time
from datetime import datetime
import sys

search_url = 'http://www.istockphoto.com/api/search'
log_file = 'istock_crawler.log'
with open(log_file, 'a') as f:
    f.write(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'))
    f.write('\n')

print('Enter the password for mysql db (user: ms_lilibeth)...')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    with open(log_file, 'a') as f:
        f.write(err_msg)
    sys.exit()

faults_count = 0


class RequestException(Exception):
    def __init__(self, message):
        self.code = 1244
        self.message = message

def get_portfolio_info(authorID):
    result = {}
    data = {"facets": {"pageNumber": 1, "perPage": 1, "abstractType": ["photos", "illustrations", "video", "audio"],
                       "order": "new", "portfolioID": [authorID],
                       "additionalAudio": "false"}}

    user_agent = {'User-agent': 'Mozilla/5.0'}
    r = requests.post(search_url, json=data, headers=user_agent)
    all_info = json.loads(r.text)
    if (all_info['status']) != 'success':
        raise RequestException("POST request failed")
    qty = all_info['data']['totalResults']
    result['authorID'] = authorID
    result['worksCount'] = qty
    if qty != 0:
        result['date_newest'] = convert_date_format(all_info['data']['results'][0]['approvalDate'])
        data['facets']['pageNumber'] = result['worksCount']
        r = requests.post(search_url, json=data)
        all_info = json.loads(r.text)
        if (all_info['status']) != 'success':
            raise RequestException("POST request failed")
        result['date_oldest'] = convert_date_format(all_info['data']['results'][0]['approvalDate'])
    else:
        result['date_newest'] = '0000-00-00'
        result['date_oldest'] = '0000-00-00'
    return result


def convert_date_format(date_str):
    # converts from MM-DD-YYYY to YYYY-MM-DD (as mysql DATE format retrieves)
    date_new = date_str[-4:] + '-' + date_str[:2] + '-' + date_str[3:5]
    return date_new

def write_to_database(info):
    # Connect to the database
    try:
        with connection.cursor() as cursor:
            sql = "DELETE FROM `p_stat` WHERE `authorID`=%s"
            cursor.execute(sql, (info['authorID']))

            sql = "INSERT INTO `p_stat` (`authorID`,`worksCount`, `date_oldest`, `date_newest`)" \
                  " VALUES (%s, %s, %s, %s)"
            cursor.execute(sql, (info['authorID'], info['worksCount'], info['date_oldest'], info['date_newest']))
    except MySQLError as e:
        err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
        print(err_msg)
        with open(log_file, 'a') as f:
            f.write(err_msg)
        return
    except KeyError:
        if 'authorID' in info:
            err_msg = 'Author %s: key error' % (info['authorID'])
        else:
            err_msg = 'write_to_database: key error, unknown author'
        print(err_msg)
        with open(log_file, 'a') as f:
            f.write(err_msg)
        return
    connection.commit()

PORTFOLIO_URLS = read_csv("istock.csv", sep=';')
for index, row in PORTFOLIO_URLS.iterrows():
    author_id = row['ID']
    info = get_portfolio_info(author_id)
    write_to_database(info)
    print('\r' + str(index), end='')
    time.sleep(0.5)

# *******************************************************

# photo
# g = GetItemInfo("http://www.istockphoto.com/photo/family-having-fun-with-karaoke-gm185255645-19667595?st=41ebff4")
# video
# g = GetItemInfo("http://www.istockphoto.com/video/hd-young-man-having-chemotherapy-at-home-gm180683564-27021440k?st=b8fb939")
#illustration
# g = GetItemInfo("http://www.istockphoto.com/ru/en/vector/textured-frame-gm469105448-62226772?redirect=true")
# print(g)







