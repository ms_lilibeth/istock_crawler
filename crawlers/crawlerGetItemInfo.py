import requests
import json
import os.path
import pandas as pd
from datetime import datetime
from time import sleep


class RequestException(Exception):
    def __init__(self, message):
        self.code = 1244
        self.message = message

class ItemCrawler():
    def __init__(self, gm_list, log_file='istock_crawler_ItemCrawler.log'):
        self.log_filepath = log_file
        if isinstance(gm_list, str):
            if os.path.isfile(str):
                tmp_list = []
                with open(str, 'r') as f:
                    for line in f:
                        tmp_list.append(line[:-1])
                self.gm_list = tmp_list
            else:
                raise ValueError("ItemCrawler: invalid filepath was passed")
        elif isinstance(gm_list, list):
            self.gm_list = gm_list
        else:
            raise TypeError("ItemCrawler: list or valid filepath must be passed")

        self.rest_url = 'https://rest.istockphoto.com/api/v1/assets/'
        self.csv_sep = ';'
        self.data = pd.DataFrame(columns=('gm_id', 'downloads', 'keywords', 'previewUrl', 'rest_info_json'))

    def __log(self, msg):
        with open(self.log_filepath, 'a') as f:
            m = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S') + ': ' + msg + '\n'
            f.write(m)

    def __save_result(self, save_filepath):
        self.data['gm_id'] = self.data['gm_id'].astype(int)
        self.data['downloads'] = self.data['downloads'].astype(int)
        self.data.to_csv(save_filepath, index=False, columns=['gm_id', 'downloads', 'previewUrl', 'keywords'])
        self.data.to_csv('raw_json.'+save_filepath, index=False, columns=['gm_id', 'rest_info_json'])

    def __get_item_info(self, gm_id):
        # получает инфу о конкретной работе, используя rest-сервис
        url = self.rest_url + str(gm_id)
        r = requests.get(url)
        if r.status_code == 200 or r.status_code == 201:
            s = json.loads(r.text)
            return {'gm_id': s['gettyMasterId'], 'downloads':  s['downloadsCount'], 'keywords': s['keywords'],
                    'previewUrl': s['thumbnails']['previewUrl'], 'rest_info_json': r.text}
        else:
            raise RequestException("gm_id %d: Get request failed" % gm_id)

    def get_result(self, save_result=True, save_filepath=None):
        self.__log("Started")
        if save_filepath == None:
            save_filepath = 'from gm' + str(self.gm_list[0]) + '.csv'
        print('Total: ', len(self.gm_list))
        index = 1
        try:
            for gm in self.gm_list:
                print('\r' + str(index), end='')
                index += 1
                try:
                    sleep(0.5)
                    r = self.__get_item_info(gm)
                except RequestException as e:
                    self.__log(e.message)
                    continue
                if r is not None:
                    self.data = self.data.append(r, ignore_index=True)
            if save_result:
                self.__save_result(save_filepath)
            self.__log("Stopped")
        except:
            if save_result:
                self.__save_result(save_filepath)
            self.__log("Stopped: exception")
            raise

        return self.data





