# nice service: https://rest.istockphoto.com/api/v1/assets/<gm_id>
import requests
import json
from datetime import datetime
import sys
import os.path
from time import sleep


class RequestException(Exception):
    def __init__(self, message):
        self.code = 1244
        self.message = message

class CrawlerPhotoIDs:
    __search_url = 'http://www.istockphoto.com/api/search'
    def __init__(self, user_id, log_filepath = 'istock_crawler_PhotoIDs.log'):
        self.log_filepath = log_filepath
        self.user_id = user_id
        self.data = []

    @staticmethod
    def portfolio_exists(user_id):
        data = {"facets": {"pageNumber": 1, "perPage": 1, "abstractType": ["photos"],
                           "order": "new", "portfolioID": [user_id],
                           "additionalAudio": "false"}}
        user_agent = {'User-agent': 'Mozilla/5.0'}
        r = requests.post(CrawlerPhotoIDs.__search_url, json=data, headers=user_agent)
        if r.status_code == 200 or r.status_code == 201:
            s = json.loads(r.text)
            if s['data']['totalResults'] == 0:
                return False, "Empty photo portfolio"
            return True, s['data']['results'][0]['contributor']['username']
        else:
            return False, "Portfolio not found"

    def __get_portfolio_page(self, page_number, per_page=200):
        # делает post-запрос на search-url и возвращает список фото на заданной странице портфолио
        result = []
        data = {"facets": {"pageNumber": page_number, "perPage": per_page, "abstractType": ["photos"],
                           "order": "new", "portfolioID": [self.user_id],
                           "additionalAudio": "false"}}

        user_agent = {'User-agent': 'Mozilla/5.0'}
        r = requests.post(CrawlerPhotoIDs.__search_url, json=data, headers=user_agent)
        all_info = json.loads(r.text)
        if not(r.status_code == 200 or r.status_code == 201):
            raise RequestException("POST request failed: %s \n data: %s" % (CrawlerPhotoIDs.__search_url, str(data)))

        total_results = all_info['data']['totalResults']
        if total_results % per_page != 0:
            page_max = total_results // per_page + 1
        else:
            page_max = total_results // per_page

        if page_number > page_max:
            return None

        for i in all_info['data']['results']:
            result.append(i['gettyId'])

        return result

    def __log(self, msg):
        with open(self.log_filepath, 'a') as f:
            m = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S') + ': ' + msg + '\n'
            f.write(m)

    def get_id_list(self, save_to_file=False, save_filepath=None):
        try:
            page_num = 1
            sleep(0.5)
            res_set = self.__get_portfolio_page(page_num)
            while res_set is not None:
                self.data.extend(res_set)
                page_num += 1
                res_set = self.__get_portfolio_page(page_num)
            if save_to_file:
                if save_filepath is None:
                    save_filepath = str(self.user_id) + '.txt'
                with open(save_filepath, 'w') as f:
                    for i in self.data:
                        s = str(i) + '\n'
                        f.write(s)
        except RequestException as e:
            self.__log(e.message)
            raise
        except:
            self.__log("Stopped: exception")
            raise
        self.__log("Stopped")
        return self.data


if __name__=='__main__':
    log_file = 'istock_crawler_GetAllPhotoIDs.log'
    if len(sys.argv) == 1:
        print("Pass author's id (or several ids) as a command line parameter")
        exit()
    for id in sys.argv[1:]:
        CrawlerPhotoIDs(id, log_file).get_id_list(save_to_file=True)


