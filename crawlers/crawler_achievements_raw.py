import requests
import pandas as pd
import json
import pymysql
from pymysql import MySQLError
import getpass
import time
from datetime import datetime
import sys
from bs4 import BeautifulSoup
import re


class ParseException(Exception):
    def __init__(self, message):
        self.code = 1244
        self.message = message

URL = 'https://www.istockphoto.com/profile/'

output_file = 'crawler_profile.log'
with open(output_file, 'a') as f:
    f.write(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'))
    f.write('\n')

print('Enter the password for mysql db (user: ms_lilibeth) \n')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    with open(output_file, 'a') as f:
        f.write(err_msg)
    sys.exit()


def get_achievements(username):
    result = {}
    url = URL + username
    r = requests.get(url)
    parsed_html = BeautifulSoup(r.text, 'html.parser')
    r.raise_for_status()  # raises requests.exceptions.HTTPError

    imgs = parsed_html.find_all('img', {'class': 'icons'})
    result = []
    for img in imgs:
        result.append(img.attrs['alt'])
    return result


def write_to_database(achievement_list, authorID):
    # Connect to the database
    with connection.cursor() as cursor:
        sql = "DELETE FROM `achievements` WHERE `authorID`=%s"
        cursor.execute(sql, authorID)
        for a in achievement_list:
            sql = "INSERT INTO `achievements` (`authorID`,`a_text`)" \
                  " VALUES (%s, %s)"
            cursor.execute(sql, (authorID, a))
    connection.commit()
# **************************
csv_info = pd.read_csv("usernames.csv")
for index, row in csv_info.iterrows():
    try:
        result = get_achievements(row['username'])
    except requests.exceptions.HTTPError:
        err_msg = 'Author %s: GET request failed' % row['username']
        print(err_msg)
        with open(output_file, 'a') as f:
            f.write(err_msg)
        continue
    try:
        write_to_database(result, row['ID'])
    except MySQLError as e:
        err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
        print(err_msg)
        with open(output_file, 'a') as f:
            f.write(err_msg)
        continue
    print('\r' + str(index), end='')
# except KeyError:
#     if 'authorID' in info:
#         err_msg = 'Author %s: key error' % (info['authorID'])
#     else:
#         err_msg = 'write_to_database: key error, unknown author'
#     print(err_msg)
#     with open(output_file, 'a') as f:
#         f.write(err_msg)
#     return
