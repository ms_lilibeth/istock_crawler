from crawlerGetAllPhotoIDs import CrawlerPhotoIDs
from crawlerGetItemInfo import ItemCrawler
from crawlerDownloadImg import download
import sys
import os

def crawl():
    log_file = 'istock_crawler_GetAllPhotoIDs.log'
    author_list = [475444]

    # if len(sys.argv) == 1:
    #     print("Pass author's id (or several ids) as a command line parameter")
    #     exit()

    for id in author_list:
        try:
            path = str(id) + '_gm_list.txt'
            print("author %d: getting list of photos" % int(id))
            l = CrawlerPhotoIDs(id, log_file).get_id_list(save_to_file=True, save_filepath=path)
            print("author %d: getting photo info" % int(id))
            path_to_csv = str(id) + '.csv'
            ItemCrawler(l).get_result(save_filepath=path_to_csv)
            #
            # print("author %d: downloading previews" % int(id))
            # if not os.path.exists(str(id)):
            #     os.makedirs(str(id))
            # download(path_to_csv, save_to=(str(id) + '/'))
        except:
            continue
    #
    # id = 1846989
    # path_to_csv = str(id) + '.csv'
    # print("author %d: downloading previews" % int(id))
    # if not os.path.exists(str(id)):
    #     os.makedirs(str(id))
    # download(path_to_csv, save_to=(str(id) + '/'))

crawl()