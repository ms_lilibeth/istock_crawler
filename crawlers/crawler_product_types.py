import requests
from pandas import read_csv
import json
import pymysql
from pymysql import MySQLError
import getpass
import time
from datetime import datetime
import sys

search_url = 'http://www.istockphoto.com/api/search'
output_file = 'istock_crawler_product_types.log'

with open(output_file, 'a') as f:
    f.write(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'))
    f.write('\n')

print('Enter the password for mysql db (user: ms_lilibeth)...')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    with open(output_file, 'a') as f:
        f.write(err_msg)
    sys.exit()

def convert_date_format(date_str):
    # converts from MM-DD-YYYY to YYYY-MM-DD (as mysql DATE format retrieves)
    date_new = date_str[-4:] + '-' + date_str[:2] + '-' + date_str[3:5]
    return date_new

type_of_content = ["photos", "illustrations", "video"]

faults_count = 0


def get_info(authorID, type):
    result = {}
    if type not in type_of_content:
        raise BaseException("Invalid type of content")
    data = {"facets": {"pageNumber": 1, "perPage": 1, "abstractType": [type],
                       "order": "new", "portfolioID": [authorID],
                       "additionalAudio": "false"}}
    try:
        r = requests.post(search_url, json=data)
        all_info = json.loads(r.text)
        if (all_info['status']) != 'success':
            raise BaseException("POST request failed")
        qty = all_info['data']['totalResults']
        if qty != 0:
            result['qty'] = qty
            result['date_newest'] = convert_date_format(all_info['data']['results'][0]['approvalDate'])
            data['facets']['pageNumber'] = result['qty']
            r = requests.post(search_url, json=data)
            all_info = json.loads(r.text)
            if (all_info['status']) != 'success':
                raise BaseException("POST request failed")
            result['date_oldest'] = convert_date_format(all_info['data']['results'][0]['approvalDate'])
        else:
            return None
    except:
        global faults_count
        if faults_count < 3:
            faults_count += 1
            time.sleep(5)
            return get_info(author_id)
        else:
            err_msg = 'Author %s: 3 POST requests failed' % (authorID)
            print(err_msg)
            with open(output_file, 'a') as f:
                f.write(err_msg)
            sys.exit()
    faults_count = 0
    return result

def write_to_database(info):
    # Connect to the database
    try:
        with connection.cursor() as cursor:
            # photos
            if info['photos'] is not None:
                sql = "DELETE FROM `photos` WHERE `authorID`=%s"
                cursor.execute(sql, (info['authorID']))

                sql = "INSERT INTO `photos` (`authorID`,`qty`, `date_oldest`, `date_newest`)" \
                      " VALUES (%s, %s, %s, %s)"
                cursor.execute(sql, (info['authorID'], info['photos']['qty'], info['photos']['date_oldest'],
                                     info['photos']['date_newest']))
            # illustrations
            if info['illustrations'] is not None:
                sql = "DELETE FROM `illustrations` WHERE `authorID`=%s"
                cursor.execute(sql, (info['authorID']))

                sql = "INSERT INTO `illustrations` (`authorID`,`qty`, `date_oldest`, `date_newest`)" \
                      " VALUES (%s, %s, %s, %s)"
                cursor.execute(sql, (info['authorID'], info['illustrations']['qty'], info['illustrations']['date_oldest'],
                                     info['illustrations']['date_newest']))
            # video
            if info['video'] is not None:
                sql = "DELETE FROM `video` WHERE `authorID`=%s"
                cursor.execute(sql, (info['authorID']))

                sql = "INSERT INTO `video` (`authorID`,`qty`, `date_oldest`, `date_newest`)" \
                      " VALUES (%s, %s, %s, %s)"
                cursor.execute(sql, (info['authorID'], info['video']['qty'], info['video']['date_oldest'],
                                     info['video']['date_newest']))
    except MySQLError as e:
        err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
        print(err_msg)
        with open(output_file, 'a') as f:
            f.write(err_msg)
        return
    except KeyError:
        if 'authorID' in info:
            err_msg = 'Author %s: key error' % (info['authorID'])
        else:
            err_msg = 'write_to_database: key error, unknown author'
        print(err_msg)
        with open(output_file, 'a') as f:
            f.write(err_msg)
        return
    connection.commit()

PORTFOLIO_URLS = read_csv("istock.csv", sep=';')
for index, row in PORTFOLIO_URLS.iterrows():
    author_id = row['ID']
    ph = get_info(author_id, "photos")
    time.sleep(0.5)
    illstr = get_info(author_id, "illustrations")
    time.sleep(0.5)
    video = get_info(author_id, "video")
    time.sleep(0.5)
    info = {'photos': ph, 'illustrations': illstr, 'video': video, 'authorID': author_id}
    write_to_database(info)
    print('\r' + str(index), end='')
