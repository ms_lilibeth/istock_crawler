import requests
import pandas as pd
import json
import pymysql
from pymysql import MySQLError
import getpass
import time
from datetime import datetime
import sys
from bs4 import BeautifulSoup
import re


class ParseException(Exception):
    def __init__(self, message):
        self.code = 1244
        self.message = message

URL = 'https://www.istockphoto.com/profile/'

output_file = 'crawler_downloads.log'
with open(output_file, 'a') as f:
    f.write(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'))
    f.write('\n')

print('Enter the password for mysql db (user: ms_lilibeth) \n')
pwd = getpass.getpass()
try:
    connection = pymysql.connect(host='localhost',
                                 user='ms_lilibeth',
                                 password=pwd,
                                 db='istock_crawler',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
except MySQLError as e:
    err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
    print(err_msg)
    with open(output_file, 'a') as f:
        f.write(err_msg)
    sys.exit()


def get_downloads(username):
    result = {}
    url = URL + username
    r = requests.get(url)
    parsed_html = BeautifulSoup(r.text, 'html.parser')
    r.raise_for_status()  # raises requests.exceptions.HTTPError

    p = parsed_html.find_all('p')
    exp = re.compile("Downloads: .*\d+.*")
    p_suitable = []
    for _p in p:
        if exp.match(_p.getText()):
            p_suitable.append(_p.getText())
    if len(p_suitable) > 1:
        raise ParseException("Something strange was found: len(p_suitable) > 1")
    if len(p_suitable) == 0:
        raise ParseException("Downloads was not found")

    ints = re.findall(r'\d+', p_suitable[0])
    if len(ints) > 1 or len(ints) == 0:
        raise ParseException("Problems while extracting downloads")
    else:
        return int(ints[0])


def write_to_database(info):
    # Connect to the database
    with connection.cursor() as cursor:
        sql = "UPDATE `p_stat` SET `downloads`=%s WHERE `authorID`=%s"
        cursor.execute(sql, (info['downloads'], info['authorID']))
    connection.commit()

csv_info = pd.read_csv("usernames.csv")
for index, row in csv_info.iterrows():
    try:
        downloads = get_downloads(row['username'])
    except requests.exceptions.HTTPError:
        err_msg = 'Author %s: GET request failed' % row['username']
        print(err_msg)
        with open(output_file, 'a') as f:
            f.write(err_msg)
        continue
    except ParseException:
        err_msg = 'Author %s: Parse exception' % row['username']
        with open(output_file, 'a') as f:
            f.write(err_msg)
        continue
    info = {'authorID': row['ID'], 'downloads': downloads}
    try:
        write_to_database(info)
    except MySQLError as e:
        err_msg = 'Got error {!r}, errno is {}'.format(e, e.args[0])
        print(err_msg)
        with open(output_file, 'a') as f:
            f.write(err_msg)
        continue
    print('\r' + str(index), end='')


