import json

DIR_D = '/home/ms_lilibeth/GIT/istock_crawler/data/downloads-week-since-2012/'
filemask_D = 'downloads-week-top-50-since-2012.json'
with open(DIR_D+filemask_D, 'r') as f:
    files_D = json.load(f)

with open(DIR_D+ 'ph-'+filemask_D, 'r') as f:
    photos_D = json.load(f)

with open(DIR_D+ 'v-'+filemask_D, 'r') as f:
    video_D = json.load(f)

with open(DIR_D+ 'ill-'+filemask_D, 'r') as f:
    illstr_D = json.load(f)

DIR_F = '/home/ms_lilibeth/GIT/istock_crawler/data/files-week-since-2012/'
filemask_F = 'files-week-top-50-since-2012.json'
with open(DIR_F+filemask_F, 'r') as f:
    files_F = json.load(f)

df_files = [it for it in files_D if it not in files_F]
print('df: ', len(df_files))
print('best both: ')
for it in [i for i in files_D if i in files_F]:
    print(it)

print('Downloads: ')
f_only_D = [it for it in files_D if it not in photos_D and it not in video_D and it not in illstr_D]
best_of_the_best_D = [it for it in photos_D if it in video_D and it in illstr_D]

print("f_only: ", len(f_only_D))
print('b_of_the_b: ', len(best_of_the_best_D))

ph_v_D = [it for it in photos_D if it in video_D and it not in best_of_the_best_D]
print('ph_v: ', len(ph_v_D))

ph_only_D = [it for it in photos_D if it not in video_D and it not in illstr_D and it not in best_of_the_best_D]
print('ph_only: ', len(ph_only_D))

ph_illstr_D = [it for it in photos_D if it in illstr_D and it not in best_of_the_best_D]
print('ph_illstr: ', len(ph_illstr_D))

v_illstr_D = [it for it in illstr_D if it in video_D and it not in best_of_the_best_D]
print('v_illstr: ', len(v_illstr_D))

v_only_D = [it for it in video_D if it not in photos_D and it not in illstr_D and it not in best_of_the_best_D]
print('v_only: ', len(v_only_D))

illstr_only_D = [it for it in illstr_D if it not in photos_D and it not in video_D and it not in best_of_the_best_D]
print('illstr_only: ', len(illstr_only_D))

print('best_of_the_best: list')
for it in best_of_the_best_D:
    print(it)